/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package alumno;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;


public class Estudiante extends JFrame {
    private DefaultTableModel tableModel;
    private JTable table;
    private JTextField textFieldNombre;    
    private JTextField  textFieldCarrera;
    
   public Estudiante() {setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Registro de Personas");
        setSize(600, 300);
        setLocationRelativeTo(null);

        // Panel principal
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        getContentPane().add(panel);

        // Panel de entrada
        JPanel panelEntrada = new JPanel();
        									//fila - columna
        // Crear un contenedor con GridLayout de 2 filas y 4 columnas
        panelEntrada.setLayout(new GridLayout(2, 4));
        panel.add(panelEntrada, BorderLayout.NORTH);

        // Etiquetas y campos de texto
        JLabel labelNombre = new JLabel("Nombre:");
        textFieldNombre = new JTextField();
        JLabel labelEdad = new JLabel("Carrera:");
        textFieldCarrera  = new JTextField();
        panelEntrada.add(labelNombre);
        panelEntrada.add(textFieldNombre);
        panelEntrada.add(labelEdad);
        panelEntrada.add(textFieldCarrera);

JButton buttonAgregar = new JButton("Agregar");
        							//formato RGB
        buttonAgregar.setBackground(new Color(9, 92, 174));
        buttonAgregar.setForeground(Color.WHITE);
        JButton buttonBuscar = new JButton("BUSCAR");
        JButton buttonActualizar = new JButton("Actualizar");
        JButton buttonEliminar = new JButton("ELIMINAR");
        panelEntrada.add(buttonAgregar);
        panelEntrada.add(buttonBuscar);
        panelEntrada.add(buttonActualizar);
        panelEntrada.add(buttonEliminar);

tableModel = new DefaultTableModel();
        tableModel.addColumn("Nombre");
        tableModel.addColumn("Carrera");
        table = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(table);
        panel.add(scrollPane, BorderLayout.CENTER);
        //accion de los botones
        buttonAgregar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombre = textFieldNombre.getText();
                String carrera = textFieldCarrera.getText();
                String[] rowData = {nombre,carrera};
                tableModel.addRow(rowData);
                textFieldNombre.setText("");
                textFieldCarrera.setText("");
            }
        });
        
        buttonBuscar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombre = textFieldNombre.getText();
                boolean encontrado = false;
                for (int i = 0; i < tableModel.getRowCount(); i++) {
                    String nombreTabla = (String) tableModel.getValueAt(i, 0);
                    if (nombreTabla.equals(nombre)) {
                        table.setRowSelectionInterval(i, i);
                        
                        table.scrollRectToVisible(table.getCellRect(i, 0, true));
                        encontrado = true;
                        break;
                    }
                }
                if (!encontrado) {
                    JOptionPane.showMessageDialog(null, "Registro no encontrado.");
                }
                else JOptionPane.showMessageDialog(null, "Registro encontrado.");
            }
        });
        
        table.getSelectionModel().addListSelectionListener((ListSelectionListener) new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int selectedRow = table.getSelectedRow();
                if (selectedRow != -1) {
                    String nombre = (String) table.getValueAt(selectedRow, 0);
                    String carrera = table.getValueAt(selectedRow, 1).toString();
                    textFieldNombre.setText(nombre);
                    textFieldCarrera.setText(carrera);
                }
            }
        });

        buttonActualizar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                if (selectedRow != -1) {
                    String nombre = textFieldNombre.getText();
                    String carrera = textFieldCarrera.getText();
                    tableModel.setValueAt(nombre, selectedRow, 0);
                    tableModel.setValueAt(carrera, selectedRow, 1);
                    textFieldNombre.setText("");
                    textFieldCarrera.setText("");
                    table.clearSelection();
                } else {
                    JOptionPane.showMessageDialog(null, "Seleccione un registro para actualizar.");
                }
            }
        });

        buttonEliminar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                if (selectedRow != -1) {
                    tableModel.removeRow(selectedRow);
                    textFieldNombre.setText("");
                    textFieldCarrera.setText("");
                    table.clearSelection();
                } else {
                    JOptionPane.showMessageDialog(null, "Seleccione un registro para eliminar.");
                }
            }
        });
    }

    

        


        


       
       
   }
    
